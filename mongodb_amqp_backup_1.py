import pymongo
import os
import json
import argparse
from datetime import datetime, timezone

def timestamp(dt):
    return int(dt.replace(tzinfo=timezone.utc).timestamp() * 1000)

def save_file_info(database, collection, new_collection_name, ts_start, ts_end):

    results = collection.aggregate([
        { "$match": { "time": {"$gte": ts_start, "$lt": ts_end}}},
        { "$project": { "_id":0,  "teloc_file": 1, "conf_issue": 1, "time": 1} },
        {"$group": {
             "_id": {"tf": "$teloc_file", "ci": "$conf_issue"},
             "max": { "$max": "$time" },
             "min": { "$min": "$time" }
            }
         },
         { "$project": { "teloc_file":"$_id.tf", "conf_issue":"$_id.ci", "_id": 0, "min": 1, "max": 1} }
    ])

    new_collection = database[new_collection_name]
    new_collection.insert_many(results)

def field_types(database, collection):

    results = collection.aggregate([

        { "$project": { "fieldType": {  "$type": "$value"  } } },
        {"$group": {
             "_id": "$fieldType" }
            }

    ])

    return results

parser = argparse.ArgumentParser(description='This is python script for exporting from and importing in mongodb')
parser.add_argument('-f','--host_from', help='Mongodb host', default='localhost', required=True)
parser.add_argument('-o', '--port', help='Mongodb port', default=27017, type=int, required=True)
parser.add_argument('-k','--host_to', help='Mongodb host', default='localhost', required=True)
parser.add_argument('-u','--username',help='Mongodb user', default='root', required=True)
parser.add_argument('-p', '--password', help='Mongodb password', required=True)
parser.add_argument('-d', '--database', help='Mongodb database', required=True)
parser.add_argument('-s', '--start_date', help='Start date for exporting, format is dd-mm-YYYY', required=True)
parser.add_argument('-e', '--end_date', help='End date for exporting, format is dd-mm-YYYY', required=True)

args = parser.parse_args()

host_from = args.host_from
port = args.port
host_to = args.host_to
username = args.username
password = args.password
database = args.database
start_date = datetime.strptime(args.start_date, '%d-%m-%Y')
end_date = datetime.strptime(args.end_date, '%d-%m-%Y')
start_time = int(start_date.strftime("%s")) * 1000
end_time = int(end_date.strftime("%s")) * 1000

month_number = start_date.month
year_number = start_date.year
list_of_vehicles = [line.strip() for line in open("vechicles_1_test.txt",'r')]

client = pymongo.MongoClient(host=host_from, port=port, maxPoolSize=2, username=username, password=password, authSource="admin")
client_new = pymongo.MongoClient(host=host_to, port=port, maxPoolSize=2, username=username, password=password, authSource="admin")

for collection in client[database].collection_names():
        #print(collection)
        if "AMQP" in collection and not "PREVIEW" in collection:
                #missing part for vehicles, when you add it, indent everything after 40 line(for loop for vehicles)
            for vehicle in list_of_vehicles:
                    vehicle = "_"+vehicle+"_"
                    if vehicle in collection in collection:
                        vehicle_collection = client[database][collection]

                        vehicle_new_id_val_collection_name = collection + "_" + str(year_number) + "_" + str(month_number)
                        field_types = field_types(client[database], vehicle_collection)
                        for field_type in field_types:
                            print(str(field_type["_id"]))
                            spark_submit_command = "spark-submit --class jobs.BackupMongoDBData --deploy-mode cluster --master yarn --num-executors 2 --executor-cores 4 --executor-memory 10g --packages org.mongodb.spark:mongo-spark-connector_2.12:3.0.1 /var/lib/spark/backupmongodata_2.12-0.1.jar " + collection + "  mongodb://" + str(username) + ":"+str(password)+"@"+str(host_from)+":"+str(port)+"/admin?authSource=admin evaplus_adl-adl " + str(start_time) + " " + str(end_time) + " MongoPaginateByCountPartitioner time mongodb://" + str(username) + ":" + str(password) + "@" + str(host_to) + ":" + str(port) + "/admin?authSource=admin " + vehicle_new_id_val_collection_name + " " + field_type["_id"]
                            os.system(spark_submit_command)
