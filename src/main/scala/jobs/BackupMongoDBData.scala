package jobs

import com.mongodb.spark.MongoSpark
import com.mongodb.spark.config.{ReadConfig, WriteConfig}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{DoubleType, StringType, TimestampType}
import org.apache.spark.sql.{SaveMode, SparkSession}
import org.bson.Document

object BackupMongoDBData extends DataLoad {
  override def execute(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.INFO)
    val LOG: Logger = Logger.getLogger(this.getClass)

    if (args.length == 0) throw new IllegalArgumentException("Give me the params")

    //example for url is jdbc:mongodb://MyUSer:tech@localhost:27017/chatme?authSource=admin

    val collection: String = args(0)
    val uri: String = args(1)
    val database: String = args(2)
    val startDate: Long = args(3).toLong
    val endDate: Long = args(4).toLong
    val partitioner: String = args(5)
    val partitionColumn: String = args(6)
    val backupUri: String = args(7)
    val idValueCollection: String = args(8)
    val castType: String = args(9)


    val spark: SparkSession = SparkSession.builder()
      .appName(s"BackupMongoDBData")
      .getOrCreate()

    val sc = spark.sparkContext

    val readConfig = ReadConfig(Map(
      "uri" -> uri,
      "database" -> database,
      "collection" -> collection,
      "partitioner" -> partitioner,
      "partitionerOptions.partitionKey" -> partitionColumn,
      "numberOfPartitions.numberOfPartitions" -> "5"
    ))

    val mongoRdd = MongoSpark.load(sc, readConfig)

    val mongoDf = mongoRdd.withPipeline(Seq(Document.parse("{$match: {time: {$gte: " + startDate + ", $lt: " + endDate+ " }, value: {$type: '" + castType + "'}}}"))).toDF()


    mongoDf.cache()

    val idAndValueDf = mongoDf
      .withColumn("metadata", map(
        lit("serial_number"), col("serial_number"),
        lit("conf_issue"), col("conf_issue"),
        lit("vehicle_id"), col("vehicle_id"),
        lit("teloc_file"), col("teloc_file"),
        lit("teloc_memory"), col("teloc_memory"),
        lit("identifier"), col("identifier"),
        lit("name"), col("name"),
        lit("sourceId"), col("sourceId"))
      )
      .withColumn("time", from_unixtime(col("time") / 1000, "yyyy-MM-dd HH:mm:ss").cast(TimestampType))
      .select("metadata", "time", "value", "distance")

    idAndValueDf
      .write
      .format("mongo")
      .mode(SaveMode.Append)
      .option("uri", backupUri)
      .option("database", database)
      .option("collection", idValueCollection)
      .save()

  }
}
