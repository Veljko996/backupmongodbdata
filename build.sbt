name := "BackupMongoData"

version := "0.1"

scalaVersion := "2.12.10"

val sparkVersion = "3.1.1"

libraryDependencies ++= Seq(
  "org.mongodb.spark" %% "mongo-spark-connector" % "3.0.1" % "compile",
  "org.apache.spark" %% "spark-core" % sparkVersion % "compile",
  "org.apache.spark" %% "spark-sql" % sparkVersion % "compile"
)
