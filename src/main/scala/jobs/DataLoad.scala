package jobs

import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, DataFrameWriter, Row}

trait DataLoad {
  def execute(args: Array[String] = Array()): Unit



  def main(args: Array[String]): Unit = {
      try {
        execute(args)
      } catch {
        case e: Throwable => {
          throw e
        }
      }
    }


}
